---
html:
  embed_local_images: true # 设置为 true，那么所有的本地图片将会被嵌入为 base64 格式
  embed_svg: true
  offline: false
  # toc: undefined # 默认是缺省的，目录会被启动，但是不会显示。可以设置为 true 或 false，来主动 显示 或 隐藏
# 自动导出设置
# export_on_save:
  # html: true
presentation:
  # theme: moon.css
  # transition: 'zoom' # none/fade/slide/convex/concave/zoom
  width: 960
  slideNumber: true
  # center: false
---



<!-- slide -->

# Welcome! 
![](./first.jpeg)  

<!-- slide -->

![](./交流.jpeg) 

<!-- slide -->

# About

- 游戏开发经历
- 游戏开发分工
- 游戏开发基础
- 关于 Cocos 引擎
- 交流讨论
- ？？？

<!-- slide -->

## 经历介绍
- Cocos2d-x C++
- Cocos Studio
![](./1.png)  


<!-- slide -->

## 经历介绍
- Cocos2d-x + lua
- Cocos Studio
- ANYSDK
@import "cc2dx.png" {width="400"}

<!-- slide -->

## 经历介绍
- Egret h5    
@import "arpg.png" {width="320" }


<!-- slide -->

## 经历介绍
- Cocos Creator
  - 消消乐
  - 俄罗斯方块
  - 弹球
  - 塔防
  - 。。。。


<!-- slide -->

## 经历介绍
- Laya
  - 依赖 unity 导出3d 场景

- Cocos Creator 3D / 3.0
  - 有自己的编辑器
  - 开始3D学习之路

<!-- slide -->
## 游戏开发分工
- 策划 (好玩)  
- 美术 (好看)  
- 程序 (可以玩)  
- 测试 (真的可以玩)
- 运营 (更多人来玩)


<!-- slide -->
## 游戏开发分工

@import "fg.png" {width="600" }

<!-- slide -->
## 游戏开发分工

@import "测试.png" {width="600" }

<!-- slide -->

# 游戏开发基础
- 数学/物理/...
- 计算机基础学科(数据结构/数据库/计算机网络/编译原理/操作系统/计组/...)
- 沟通合作

<!-- slide -->

## 数学
已知各个飞机的坐标和黑色飞机的朝向。
其他飞机与黑色飞机前后左右的关系？
@import "向量.png" {width="600" }

<!-- slide -->

## 数学
- 向量  
  - 点乘 -> 前后
  - 叉乘 -> 左右
@import "向量2.png" {width="600" }


<!-- slide -->

## 数学
- 向量综合计算
[折纸效果](https://mp.weixin.qq.com/s/1guPBbKkG6iWCcWa_uz6CQ)  

@import "折纸.gif" {width="600" }

<!-- slide -->

## 数学 
- 矩阵  
  - SRT
  - 对向量的变换/对坐标系的变换
@import "矩阵1.png" {width="600" }

<!-- slide -->

## 数学 
- 矩阵  
  - 二维中的旋转矩阵乘两次
$$
\begin{bmatrix}
   \cos{\theta} & -\sin{\theta} \\
   \sin{\theta} & \cos{\theta}
\end{bmatrix}^{2}= \\
\begin{bmatrix}
   \cos^{2}{\theta}-\sin^{2}{\theta} & -2\sin{\theta}\cos{\theta} \\
   2\sin{\theta}\cos{\theta} & \cos^{2}{\theta}-\sin^{2}{\theta}
\end{bmatrix}^{2} \\
= \begin{bmatrix}
   \cos{2\theta} & -\sin{2\theta} \\
   \sin{2\theta} & \cos{2\theta}
\end{bmatrix}\\
$$

<!-- slide -->

## 数学 
- 矩阵  
  - 多层级父子关系，存储了相对父节点的变化矩阵
@import "矩阵2.png" {width="400" }

<!-- slide -->

## 数学 
- 矩阵  
  - MVP
  - 世界坐标->观察坐标->投影坐标
@import "矩阵3.png" {width="600" }

<!-- slide -->

## 数学 
- [四元数与3D旋转](https://mp.weixin.qq.com/s/zwF5PcR96gazP1k-IzXEPg)  

@import "四元数.gif" {width="600" }

<!-- slide -->

@import "四元数比较.png" 

<!-- slide -->

## 数学
- [物体随机飞溅运动](https://mp.weixin.qq.com/s/Qu9Uy55KvUX5sSLt_PTUJQ) 
@import "物体随机飞溅运动.gif" {width="600" }

<!-- slide -->

- [物体随机飞溅运动](https://mp.weixin.qq.com/s/Qu9Uy55KvUX5sSLt_PTUJQ) 
@import "物体随机飞溅运动3.jpg" {width="1000" }
@import "物体随机飞溅运动2.jpg" {width="600" }

<!-- slide -->

### 物理
#### [​如何实现高抛平抛发射](https://mp.weixin.qq.com/s/5GgL_pONl0bQPxFz4xtjmQ)

@import "高抛平抛发射.gif" {width="600" }


<!-- slide -->

#### [​如何实现高抛平抛发射](https://mp.weixin.qq.com/s/5GgL_pONl0bQPxFz4xtjmQ)

确认已知条件：
- 起点发射速度大小 V
- 重力加速 G
- 起始点与经过点

需要求出：
- 发射角度 a

<!-- slide -->

#### [​如何实现高抛平抛发射](https://mp.weixin.qq.com/s/5GgL_pONl0bQPxFz4xtjmQ)

@import "高抛平抛发射2.png" {width="400" }


<!-- slide -->

#### [转向行为AI](https://mp.weixin.qq.com/s/TOAfkeNBDb6NdOqRqzJhwQ)

@import "转向行为1.gif" {width="600" }


<!-- slide -->

#### [转向行为AI](https://mp.weixin.qq.com/s/TOAfkeNBDb6NdOqRqzJhwQ)

@import "转向行为2.jpg" {width="600" }

<!-- slide -->

#### [​基础光照模型！](https://mp.weixin.qq.com/s/RtARzTb9KahZ70Ct5r8GRw)

- 环境(Ambient)
- 漫反射(Diffuse)
- 镜面高光(Specular)

@import "光照1.gif" {width="400" }


<!-- slide -->


#### [​基础光照模型！](https://mp.weixin.qq.com/s/RtARzTb9KahZ70Ct5r8GRw)


@import "光照2.png" {width="700" }

<!-- slide -->

#### [​游戏背后的“数学”本质？听听零零后主程怎么说](https://mp.weixin.qq.com/s/hk1r1QAkBG3Su1Gr7_dmEg)


@import "pi.png" 


<!-- slide -->

### 基础学科
#### 计算机
- 输入，运算，输出
- 软件是指令和数据的集合
- 对计算机来说什么都是数字

<!-- slide -->

### 基础学科
#### 算法
> 程序设计的熟语
- 辗转相除
- 二分查找
- 冒泡排序
- 快速排序

<!-- slide -->

@import "辗转相除.png"  

<!-- slide -->

@import "二分查找.gif"  

<!-- slide -->

@import "冒泡.gif"  

<!-- slide -->

@import "快速排序.gif"  

<!-- slide -->

### 基础学科
#### 数据结构
- 数组
- 栈
- 队列
- 链表
- 二叉树


<!-- slide -->

@import "stack.png"  


<!-- slide -->

@import "tree.png" {width="600" }


<!-- slide -->

[四叉树](https://mp.weixin.qq.com/s/gkvOd11kbZYcKXkBc7V8kQ)  
@import "四叉树1.gif" {width="600" }

<!-- slide -->


[四叉树](https://mp.weixin.qq.com/s/gkvOd11kbZYcKXkBc7V8kQ)  
- [https://github.com/timohausmann/quadtree-js](https://github.com/timohausmann/quadtree-js)  

@import "四叉树2.gif" {width="600" }



<!-- slide -->

### 基础学科

#### 语言

- 解释语言  JavaScript
- 编译语言  C++


<!-- slide -->

##### 语言
###### 语言之间交互

js <--> cpp <--> java

<!-- slide -->

### 基础学科

- 计算机网络
- 数据库
前端<-->后端<-->数据库



<!-- slide -->

### 基础
#### 好处
- 考研/保研
- 面适
- 工作
- 认识事物规律


<!-- slide -->

# 关于 Cocos 引擎

@import "让游戏开发更简单.png" {width="1000" }

<!-- slide -->

# 关于 Cocos 引擎

[星旅 StarTrek ](https://mp.weixin.qq.com/s/KktBByqjXIB8-tAgFp6N0g)  

@import "星旅.png" {width="1000" }


<!-- slide -->

[Plotter - 一个科学计算的程序](https://mp.weixin.qq.com/s/MkPrtwxdgxI32XXAoiZtVw)  
@import "Plotter.png" {width="800" }



<!-- slide -->

## 活跃的社区 -- 宝藏学习之地

@import "论坛.png" {width="1000" }


<!-- slide -->

## [开源的代码](https://docs.cocos.com/creator/manual/zh/submit-pr/submit-pr.html) -- 更深入的探索之路
- [JavaScript 引擎 ](https://github.com/cocos-creator/engine)
- [Cocos2d-x 引擎 ](https://github.com/cocos-creator/engine-native) 
- [jsb-adapter](https://github.com/cocos-creator-packages/jsb-adapter) 
- [Mini-game-adapters](https://github.com/cocos-creator-packages/adapters/)


<!-- slide -->

## 强强联合 - 与[华为](https://docs.cocos.com/creator/3.3/manual/zh/editor/publish/publish-huawei-ohos.html)等深度合作

@import "发布平台.png" {width="600" }


<!-- slide -->
# Thanks

- 交流讨论

@import "Cocos.png" 

<!-- slide -->
# 小作业

- 跳一跳
